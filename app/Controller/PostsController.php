<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PostsController
 *
 * @author rahul
 */
App::uses('AppController', 'Controller');

class PostsController extends AppController{
    //put your code here
    public $uses = array('Posts','Elections');
    public $scaffold = 'admin';
    public $components = array('ElectionValidity');
    public function add(){
        if($this->request->is('post')){
            $this->Posts->set($this->request->data);            
            if($this->Posts->validates()){                
                $this->ElectionValidity->checkEditibility($this->request->data['Posts']['election_id']);
                $this->Posts->set('id');
                $this->Posts->set('order',0);
                $this->layout = 'ajax';                
                if($data = $this->Posts->save(false)){                    
                    $this->Session->setFlash("Post is created successfully",'success_flash',array());
                }
                else{
                    $this->Session->setFlash("Some error occurred, please try again",'error_flash',array());
                }
            }
            else{                                                
                $this->Session->setFlash("Please check the field values",'error_flash',$this->Posts->invalidFields());
            }
            $this->redirect(array('controller'=>'elections','action'=>'edit',$this->request->data['Posts']['election_id']));        
        }
        else{
            throw new BadRequestException();
        }
    }
}
