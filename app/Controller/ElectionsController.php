<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ElectionsController
 *
 * @author rahul
 */
App::uses('AppController', 'Controller');

class ElectionsController extends AppController{        
    public function beforeFilter() {
            parent::beforeFilter();
            $this->Auth->allow('view','check_voter_cred','authElection');
    }

    public $uses = array('Elections','Candidates','Posts','Voters');
    public $helpers = array('Form','Paginator','Time','Html');
    public $scaffold = 'admin';
    public $components = array('ElectionValidity');
    
    public function add(){
        if($this->request->is('post')){
            $user = $this->Auth->user();
            $this->Elections->set($this->request->data);
            $this->Elections->set('admin_id',$user['Users']['id']);
            $this->Elections->set('mode','editing');
            $this->Elections->set('structure',"");
            $this->Elections->set('id');
            if($this->Elections->save()){
                $this->Session->setFlash("Election is created successfully",'success_flash',array(),'addelection');
            }
            else{
                $this->Session->setFlash("Error occurred, please try again",'error_flash',array(),'addelection');
            }
            $this->redirect(array('controller'=>'elections','action'=>'index'));
        }
        else{
            throw new BadRequestException();
        }
    }

    public function passcode($id,$what){
        $this->Elections->unbindModelAll();
        $elec = $this->Elections->findByid($id);
        if(!$elec)
            throw new NotFoundException();
        $this->ElectionValidity->checkUser($elec);
        if($elec['Elections']['mode'] != 'over'){
            if($what == 'generate'){
                $passcode = mt_rand(100000000,999999999);
                $elec['Elections']['passcode'] = $passcode;
                $this->Elections->save($elec,false);
            }
            else if($what == 'remove'){
                $elec['Elections']['passcode'] = 0;
                $this->Elections->save($elec,false);    
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    public function edit($id){  
        if($this->request->is('post')){            
            if(isset($this->request->data['Elections'])){
                $this->Elections->recursive = 0;
                $elec = $this->Elections->findByid($id);
                $this->ElectionValidity->checkEditibility($elec);
                $elec['Elections']['title'] = $this->request->data['Elections']['title'];
                $elec['Elections']['desc'] = $this->request->data['Elections']['desc'];
                if($this->Elections->save($elec['Elections']))
                    $this->Session->setFlash("Election detail updated successfully",'success_flash',array());
            }
            else if(isset($this->request->data['candidatesList'])){
                $data = json_decode(urldecode($this->request->data['candidatesList']));
                $this->ElectionValidity->checkEditibility($id);
                $this->Candidates->recursive = 0;
                $candidates = $this->Candidates->find('list',array(
                        'fields' => array('Candidates.id'),
                        'conditions' => array('Candidates.election_id' => $id)                        
                    ));
                $this->Posts->recursive = 0;
                $posts = $this->Posts->find('list',array(
                        'fields' => array('Posts.id'),
                        'conditions' => array('Posts.election_id' => $id)                        
                    ));
                $flag = true;
                foreach($data as $post){
                    foreach ($post as $key => $value) {                        
                        if(!isset($posts[$key]) && $key >= 0){
                            $flag = false;                        
                        }
                        unset($posts[$key]);                        
                        foreach ($value as $cand){
                            if(!isset($candidates[$cand])){
                                $flag = false;                        
                            }
                            unset($candidates[$cand]);                        
                        }
                    }
                }
                if(!$flag || count($posts) > 0 || count($candidates) > 0){
                    $this->Session->setFlash("Input Data is not correct",'error_flash',array());       
                }
                else{                    
                    $flag = true;
                    $count = 0;
                    foreach($data as $post){
                        foreach ($post as $key => $value) {                        
                            if($key >= 0){
                                if(!$this->Posts->save(array('id' => $key,'order' => $count),false))
                                    $flag = false;
                                $count++;
                            }                            
                            foreach ($value as $key1 => $cand){
                                if($key >= 0){
                                    if(!$this->Candidates->save(array('id' => $cand,'order' => $key1,'post_id' => $key),false))
                                        $flag = false;                             
                                }
                                else{
                                    if(!$this->Candidates->save(array('id' => $cand,'order' => $key1, 'post_id' => null),false))
                                        $flag = false;                                 
                                }
                            }
                        }
                    }
                    if($flag){                    
                         $this->Session->setFlash("Successfully saved",'success_flash',array());       
                    }   
                    else{                        
                        $this->Session->setFlash("Error occurred, Please try again",'error_flash',array());       
                    }
                }
            }
        }

        $this->Posts->unbindModel(array('belongsTo' => array('Election')),true);
        $this->Elections->recursive = 2;
        $elec = $this->Elections->findByid($id);
        if (!$elec) {
            throw new NotFoundException();
        }
        $this->ElectionValidity->checkEditibility($elec);            
        
        $this->Candidates->unbindModelAll();
        $unlisted = $this->Candidates->find('all',array(
            'conditions' => array('Candidates.election_id' => $id,'Candidates.post_id' => NULL)
        ));
        $this->layout = 'main';
        $this->set('title_for_layout',"election edit");
        $this->set('election_detail',$elec);
        $this->set('unlisted',$unlisted);
    }


    public function check_voter_cred($id){
        if($this->request->is('post')){
            $input = $this->request->data['Auth'];
            if($this->check_voter($input['ldap_id'],$input['password'],$input['secret_key'],$id)){
                $this->set('Success',true);
            }else{
                $this->set('Success',false);
            }
            $this->layout = 'ajax';
        }else{
            throw new BadRequestException();            
        }    
    }

    private function check_voter($userid, $userpass,$secret_key,$election_id){
        $ldap_uid = mysql_escape_string($userid);
        $ldap_pass = mysql_escape_string($userpass);
        $ds = ldap_connect("ldap.iitb.ac.in");
        if (!$ds) {
            throw new InternalErrorException("Unable to connect to LDAP server. Please try again later.");
        }
        $sr = ldap_search($ds, "dc=iitb,dc=ac,dc=in", "(uid=$ldap_uid)");
        $info = ldap_get_entries($ds, $sr);

        $do_bind = false;
        if(isset($info[0]['dn'])){
            $ldap_uid = $info[0]['dn'];        
            //$do_bind = @ldap_bind($ds, $ldap_uid, $ldap_pass);
		$do_bind = true;
        }

        if($do_bind){
            $rollnumber = strtolower($info[0]['employeenumber'][0]);
            $voter = $this->Voters->find('first',array('conditions'=>array('voter_id' => $rollnumber, 'election_id' => $election_id)));
            if(count($voter) == 0){
                $this->Session->setFlash("You are not listed as voter in the election. please contact election officers",'error_flash',array(),'auth');    
                return false;
            }
            else{
                if($voter['Voters']['is_voted']){
                    $this->Session->setFlash("Seems like you have already voted for this election",'error_flash',array(),'auth');    
                    return false;       
                }
                else if ($voter['Voters']['secret'] != $secret_key){
                    $this->Session->setFlash("Please enter a valid secret key",'error_flash',array(),'auth');
                    return false;
                }
            }
            $voter_type = strtolower($info[0]['employeetype'][0]);
            if($voter_type == "ug" || $voter_type == "dd" || in_array(substr($rollnumber,0,3),array("125","135","12i","13i","12k","13k"))){
                $voter_type = "ug";
            }
            return array('rollnumber' => $rollnumber,'type' => $voter_type);
        }else{
            $this->Session->setFlash("Username and password is not correct",'error_flash',array(),'auth');
            return false;
        }
    }

    private function add_votes($input,$voterdetail,$election){
        $valid_input = true;
        $success_update = true;
        $cands = $this->Candidates->getDataSource();
        $cands->begin();
        foreach ($election['Posts'] as $post) {
            if ((strpos($post['title'],'[UG]') !== false && $voterdetail['type'] === 'ug') || (strpos($post['title'],'[PG]') !== false && $voterdetail['type'] !== 'ug') || (strpos($post['title'],'[PG]') === false && (strpos($post['title'],'[UG]') === false)) ) {

                foreach ($post['Candidates'] as $cand) {
                    $vote = "neutral";
                    if(count($post['Candidates']) > $post['max_posts']){
                        if(isset($input[$post['id']]) && is_array($input[$post['id']])){
                            if(count($input[$post['id']]) > $post['max_posts']){
                                $valid_input = false;break;
                            }
                            if(in_array($cand['id'], $input[$post['id']]))
                                $vote = "yes";
                            else if(count($input[$post['id']]) > 0)
                                continue;
                        }                    
                    }
                    else{
                        if(isset($input[$post['id']][$cand['id']])){
                            if(in_array($input[$post['id']][$cand['id']],array("yes","no","neutral")))
                                $vote = $input[$post['id']][$cand['id']];
                            else{
                                $valid_input = false;break;    
                            }
                        }
                    }

                    if(!$this->Candidates->updateAll(array('Candidates.votes_'.$vote => "Candidates.votes_".$vote."+ 1"),array('Candidates.id' => $cand['id']))){
                        $success_update= false; break;
                    }

                }
            }
            if(!$valid_input || !$success_update)
                break;
        }
        $voterds = $this->Voters->getDataSource();
        $voterds->begin();
        if(!$this->Voters->updateAll(array('Voters.is_voted' => true),
            array('Voters.voter_id' => $voterdetail['rollnumber'], 'Voters.election_id' => $election['Elections']['id'])))
            $success_update = false;

        if($valid_input && $success_update){
            $voterds->commit();
            $cands->commit();
            return true;
        }
        else{
            $this->Session->setFlash('Some error occured try again','error_flash');
            $voterds->rollback();
            $cands->rollback();
            return false;
        }
    }

    public function view($id){            
        $this->Posts->unbindModel(array('belongsTo' => array('Election')),true);
        $this->Elections->recursive = 2;
        $elec = $this->Elections->findByid($id);
        if (!$elec) {
            throw new NotFoundException();
        }
        if($elec['Elections']['mode'] != "open")        
            throw new NotFoundException();

        if($elec['Elections']['passcode'] != 0){            
            if($this->Session->check('AuthElection')){
                if($this->Session->read('AuthElection') != $id){
                    $this->redirect(array('action'=>'authElection',$id));
                }
            }
            else{                
                $this->redirect(array('action'=>'authElection',$id));
            }
        }

        if($this->request->is('post')){
            if(isset($this->request->data['Votes'])){
                $input = $this->request->data['Votes'];            
                if($voterdetail = $this->check_voter($input['ldap_id'],$input['password'],$input['secret_key'],$id)){
                    if($this->add_votes($input,$voterdetail,$elec)){    
                        $this->set('voted',true);                        
                    }
                }
            }
        }

        $this->layout = 'main';
        $this->set('title_for_layout',$elec['Elections']['title']);
        $this->set('election_detail',$elec);
    }

    public function authElection($id){
        if($this->request->is('post')){
            $elec = $this->Elections->findByid($id);
            if($elec && isset($this->request->data['passcode'])){
                if($elec['Elections']['passcode'] == $this->request->data['passcode']){
                    $this->Session->write('AuthElection',$id);
                    $this->redirect(array('action'=>'view',$id));                    
                }
                else{
                    $this->Session->setFlash("Passcode is not correct",'error_flash');
                }
            }
        }
        $this->layout = 'main';
        $this->set('title_for_layout','Election Auth');        
    }

    public function open($id){
        $this->Elections->unbindModelAll();
        $elec = $this->Elections->findByid($id);
        $this->ElectionValidity->checkUser($elec);
        if($elec['Elections']['mode'] != "over" && $elec['Elections']['mode'] != "open"){
            $elec['Elections']['mode'] = "open";
            if($this->Elections->save($elec,false))
                $this->Session->setFlash("Successfully open the election {$elec['Elections']['title']}",'success_flash',array());       
            else
                $this->Session->setFlash("Some error occured, try again",'error_flash',array());       
        }
        $this->redirect(array('action'=>'index'));
    }

    public function close($id){
        $this->Elections->unbindModelAll();
        $elec = $this->Elections->findByid($id);
        $this->ElectionValidity->checkUser($elec);
        if($elec['Elections']['mode'] != "over" && $elec['Elections']['mode'] == "open"){
            $elec['Elections']['mode'] = "close";            
            if($this->Elections->save($elec,false))
                $this->Session->setFlash("Successfully closed the election {$elec['Elections']['title']}",'success_flash',array());       
            else
                $this->Session->setFlash("Some error occured, try again",'error_flash',array());       
        }        
        $this->redirect(array('action'=>'index'));
    }

    public function over($id){
        $this->Elections->unbindModelAll();
        $elec = $this->Elections->findByid($id);
        $this->ElectionValidity->checkUser($elec);
        if($elec['Elections']['mode'] != "over" && $elec['Elections']['mode'] != "editing"){
            $elec['Elections']['mode'] = "over";            
            if($this->Elections->save($elec,false))
                $this->Session->setFlash("Successfully finished the election {$elec['Elections']['title']}",'success_flash',array());       
            else
                $this->Session->setFlash("Some error occured, try again",'error_flash',array());       
        }
        $this->redirect(array('action'=>'index'));    
    }

    public function results($id){        
        $this->Posts->unbindModel(array('belongsTo' => array('Election')),true);
        $this->Elections->recursive = 2;
        $elec = $this->Elections->findByid($id);
        $this->ElectionValidity->checkUser($elec);

        if($elec['Elections']['mode'] != "over"){            
            throw new NotFoundException("Election is not over yet");    
        }        
        $this->layout = 'main';
        $this->set('title_for_layout','Results of election : '.$elec['Elections']['title']);        
        $this->set('election',$elec);
    }
    
    public function index(){
        $this->Elections->unbindModelAll(true);        
        $user = $this->Auth->user();
        $this->Paginator->settings = array(
            'Elections' => array(
                'limit' => 5,
                'order' => array('createdOn' => 'DESC'),
            )
        );
        
        $data = $this->paginate('Elections',array('Elections.admin_id' => $user['Users']['id']));
        $this->set('title_for_layout',"List of Elections");
        $this->set('elections_list',$data);
        $this->layout = 'main';
    }
    //put your code here
}
