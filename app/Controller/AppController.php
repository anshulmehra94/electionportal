<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $components = array(
        'Auth' => array(
            'authenticate' => array(
                'Ldap' => array(
                    'userModel' => 'Users'                    
                )                
             ),
            'loginAction' => array(
                'controller' => 'users',
                'action' => 'login',
                'plugin' => false,
                'admin' => false,
            ),            
        ),
        'Session',
        'Paginator'
    );
    
    public function beforeScaffold($method) {
        parent::beforeScaffold($method);                
        $user = $this->Auth->user(); 
        if(!( $user && $user['Users']['level'] == "1")){
            $this->Auth->logout();
            $this->Session->setFlash(
                    __('You need to be admin to access this page'),
                    'error_flash',
                    array(),
                    'auth1'
                );
            $this->redirect($this->Auth->redirectUrl());
        }
        return true;
    }
}
