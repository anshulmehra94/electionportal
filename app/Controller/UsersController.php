<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdminController
 *
 * @author rahul
 */
App::uses('AppController', 'Controller');

class UsersController extends AppController{
    //put your code here 
    
    public $uses = array('Users');
    public $scaffold = 'admin';            
    
    public function login(){                
        if ($this->request->is('post')) {            
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirect());                
            } else {
                $this->Session->setFlash(
                    __('Username or password is incorrect'),
                    'error_flash',
                    array(),
                    'auth1'
                );
            }
        }
        $this->layout = 'main';
        $title_for_layout = 'Login';
        $this->set(compact('title_for_layout'));
    }
    public function logout()
    {
        if($this->Auth->user())
        {
            $this->redirect($this->Auth->logout());
        }
        else
        {
            $this->redirect(array('controller'=>'pages','action' => 'display','home'));
            $this->Session->setFlash(__('Not logged in'), 'default', array(), 'auth');
        }
    }
}
