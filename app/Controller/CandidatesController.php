<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CandidatesController
 *
 * @author rahul
 */
App::uses('AppController', 'Controller');

class CandidatesController extends AppController{
    //put your code here
    public $uses = array('Candidates','Posts','Elections');
    public $scaffold = 'admin';
    public $components = array('ElectionValidity');
    public function add(){
        if($this->request->is('post')){
            if(isset($this->request->data['Candidates']['image_path'])){
                $file = $this->request->data['Candidates']['image_path'];
                $this->request->data['Candidates']['image_path'] = "";
            }
            $this->Candidates->set($this->request->data);            
            if($this->Candidates->validates()){                
                $election_id = $this->request->data['Candidates']['election_id'];
                $this->ElectionValidity->checkEditibility($election_id);
                if(isset($file) && $file['error'] <= 0){
                    $uploaddir = WWW_ROOT.'images_uploads/'.$election_id;                   
                    if (!file_exists($uploaddir)) 
                        mkdir($uploaddir, 0777);

                    $filename = $file['name'];
                    $count = 0;
                    while(file_exists($uploaddir.'/'.$count.'_'.$filename)){                        
                        $count++;
                    }
                    $filename = $count.'_'.$filename;
                    
                    if(!copy($file['tmp_name'], $uploaddir."/".$filename)){
                        $this->Session->setFlash("Error while saving the photo, try again",'error_flash',array());     
                        $skip = true;
                    }
                    $this->Candidates->set('image_path','images_uploads/'.$election_id.'/'.$filename);    
                }
                if(!isset($skip)){
                    $this->Candidates->set('id');
                    $this->Candidates->set('votes_yes',0);
                    $this->Candidates->set('votes_no',0);
                    $this->Candidates->set('votes_neutral',0);
                    $this->Candidates->set('order',0);
                    $this->Candidates->set('post_id',NULL);
                    $this->layout = 'ajax';                
                    if($data = $this->Candidates->save(array('validate' => false))){                    
                       $this->Session->setFlash("Candidate is added successfully",'success_flash',array());
                    }
                    else{
                       $this->Session->setFlash("Some error occurred, please try again",'error_flash',array());
                    }
                }
                    
            }
            else{                                
                $this->Session->setFlash("Please check the field values",'error_flash',$this->Candidates->invalidFields());
            }
            $this->redirect(array('controller'=>'elections','action'=>'edit',$this->request->data['Candidates']['election_id']));
        }
        else{
            throw new BadRequestException();
        }
    }
}
