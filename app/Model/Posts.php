<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Posts
 *
 * @author rahul
 */
App::uses('AppModel', 'Model');

class Posts extends AppModel{
    //put your code here
    public $tablePrefix = 'portal_';
    public $_schema = array(
        'id' => array(
            'type' => 'integer',
            'length' => 10,            
        ),
        'title' => array(
            'type' => 'string',
            'length' => 100
        ),        
        'election_id' => array(
            'type' => 'integer',
            'length' => 10,
        ),        
        'max_posts' => array(
            'type' => 'integer',
            'length' => 3
        ),
        'order' => array(
            'type' => 'integer'          
        )
    );
    public $belongsTo = array (
        'Election' => array(
            'className' => 'Elections',
            'foreignKey' => 'election_id'
        )
    );
    public $hasMany = array(
        'Candidates' => array(
            'className' => 'Candidates',
            'foreignKey' => 'post_id',
            'order' => 'Candidates.order ASC'
        )
    );
    
    public $primaryKey = 'id';
    public $validate = array(
        'title' => array(
            'alphaNumeric' => array(
                'rule'     => array('custom', '/^[a-z0-9 ,.\[\]]*$/i') ,
                'required' => true,
                'message'  => 'Alphabets and Numbers only'
            ),
            'length' => array(
                'rule' => array('maxlength',100),
                'message' => 'max length of title can be 100'
            )
        ),
        'max_posts' => array(
            'decimal' => array(
                'required' => true,
                'rule' => '/^[0-9]{1,2}$/i',            
                'message' => 'Num of Posts should be integer <= 99 '
             )
        )
    );
}
