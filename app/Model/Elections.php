<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Elections
 *
 * @author rahul
 */

App::uses('AppModel', 'Model');

class Elections extends AppModel {
    //put your code here
    public $tablePrefix = 'portal_';
    public $_schema = array(
        'id' => array(
            'type' => 'integer',
            'length' => 10,            
        ),
        'title' => array(
            'type' => 'string',
            'length' => '100'
        ),
        'desc' => array(
            'type' => 'text'
        ),
        'admin_id' => array(
            'type' => 'integer',
            'length' => 10,
        ),
        'mode' => array(
            'type' => 'string',
            'length' => 20            
        ),     
        'createdOn' => array(
            'type' => 'timestamp'
        ),
        'passcode' => array(
            'type' => 'integer',
            'length' => 20
        )        
    );
    public $belongsTo = array (
        'Admin' => array(
            'className' => 'Users',
            'foreignKey' => 'admin_id'
        )
    );
    public $hasMany = array(        
        'Posts' => array(
            'className' => 'Posts',
            'foreignKey' => 'election_id',
            'order' => 'Posts.order ASC'
        )
    );
    public $primaryKey = 'id';
    public $validate = array(
        'title' => array(
            'alphaNumeric' => array(
                'rule'     => array('custom', '/^[a-z0-9 ,.]*$/i'),
                'required' => true,
                'message'  => 'Alphabets and numbers only'
            )
        ),
        'desc' => array(
            'alphaNumeric' => array(
                'rule'     => array('custom', '/^[a-z0-9 ,.]*$/i'),                
                'message'  => 'Alphabets and numbers only',
                'allowEmpty' => true
            ),
        )
        
    );
}
