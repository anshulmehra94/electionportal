<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		//echo $this->Html->css('cake.generic');
                echo $this->HTML->css('bootstrap.min.css');
                echo $this->HTML->css('elections.css');
                echo $this->HTML->css('jquery-ui.min.css');                
                echo $this->HTML->script('jquery.min.js');
                echo $this->HTML->script('bootstrap.min.js');
                echo $this->HTML->script('jquery-ui.min.js');  
            
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div class="container">
		<div class="header">			
                    <h1 class="title">Institute Election Portal</h1>
		</div>
		<div class="content">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<div class="footer">			
		</div>
	</div>
	<?php //echo $this->element('sql_dump'); ?>        
</body>
</html>
