<h4>Upload Voters List</h4>
<h5>Upload csv file containing roll number and other detail, this file will be merged with the existing list</h5>
<hr>
<div class="row">
<div class="col-sm-6 col-sm-offset-1">
	<form role="form" method="post" enctype="multipart/form-data">  
	  <div class="form-group">
	    <label for="inputfile">Voters List(.csv)</label>
	    <input type="file" id="inputFile" name="data[Upload][file]" required="required" accept="text/csv">
	    <p class="help-block">
	    	Select csv file of voters list and specify the column no rollnumber corresponds to 
	    </p>
	  </div>
	  <div class="checkbox">
	    <label>
	      <input type="checkbox" name="data[Upload][ignore]"> Ignore first row of file
	    </label>
	  </div>
	  <div class="form-group">
	    <label for="rollnunmberfield">Column number corresponding to Roll Number</label>
	    <input type="number" class="form-control" id="rolenumberfield" name="data[Upload][column_no]" required="required" style="width:100px">
	  </div>
	  <button type="submit" class="btn btn-default">Upload</button>
	</form>
</div>
</div>
<hr>
<h4>Download Voters List</h4>
<h5>File will be downloaded in csv format in which last column contains secret pass key for each voter</h5>
<hr>
<div class="row">
<div class="col-sm-6 col-sm-offset-1">
	<form role="form" method="post" action="<?php echo $this->Html->url(array('controller' => 'voters','action' => 'download'))?>" target="_blank">  	  	
	  <div class="form-group">
	    <label for="rollnunmberfield">Match String</label>
	    <input type="text" class="form-control" id="rolenumberfield" name="data[Download][match_string]" >
	    <p class="help-block">
	    	Specify the match string which will used to filter the list eg `hostel5` if there exist hostel5 in the record. Empty will result into download of complete list
	    </p>
	  </div>
	  <input type="hidden" name="data[Download][election_id]" value="<?php echo $election['Elections']['id'] ?>">
	  <button type="submit" class="btn btn-default">Download</button>
	</form>
</div>
</div>
<hr>
<h4>Delete Voters List</h4>
<hr>
<div class="row">
<div class="col-sm-6 col-sm-offset-1">
	<form role="form" method="post" action="<?php echo $this->Html->url(array('controller' => 'voters','action' => 'delete'))?>" target="_blank">  	  	
	  <div class="form-group">
	    <label for="rollnunmberfield">Match String</label>
	    <input type="text" class="form-control" id="rolenumberfield" name="data[Delete][match_string]" >
	    <p class="help-block">
	    	Specify the match string which will used to filter the list eg `hostel5` if there exist hostel5 in the record. Empty will result into deletion of complete list
	    </p>
	  </div>
	  <input type="hidden" name="data[Delete][election_id]" value="<?php echo $election['Elections']['id'] ?>">
	  <button type="submit" class="btn btn-default">Delete</button>
	</form>
</div>
</div>
<hr>