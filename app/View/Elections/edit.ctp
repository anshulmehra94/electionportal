<?php 
    //var_dump($election_detail);        
?>
<div class="panel-group" id="accordion">
    <div id="updateElectionForm" class="panel panel-default inlineforms">
        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Change Election Title and Description</div>
        <div id="collapseOne" class="panel-collapse collapse in">
            <div class="panel-body">
            <?php 
            echo $this->Form->create('Elections',array('class' => 'form-inline',
                                                'url' => array('action' => 'edit',$election_detail['Elections']['id']),
                                                'inputDefaults' => array(
                                                    'format' => array('before', 'label', 'between','error','input','after'),
                                                    'div' => array('class' => 'form-group'),                                            
                                                    'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                                                    'class' => 'form-control'
                                                )));
            echo $this->Form->input('title',array('default' => $election_detail['Elections']['title']));
            echo $this->Form->input('desc',array('rows'=>3,'default' => $election_detail['Elections']['desc'],'label'=>array('text' => 'Descrption')));
            echo $this->Form->end(array('label' => 'Update','div'=>false,'class'=>'btn btn-default','id' => 'updateElectionDetail'));
            ?>
            </div>
        </div>
    </div>
    <div id="addPostForm" class="panel panel-default inlineforms">
        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Insert a new Post</div>        
        <div id="collapseTwo" class="panel-collapse collapse">
            <div class="panel-body">        
            <h5>Use `[UG]` / `[PG]` in the Name of the post to differentiate between UG ans PG posts</h5>
            <hr>
            <?php
            echo $this->Form->create('Posts',array('class' => 'form-inline',
                                                'url' => array('controller' => 'Posts','action' => 'add'),
                                                'inputDefaults' => array(
                                                    'format' => array('before', 'label', 'between','error','input','after'),
                                                    'div' => array('class' => 'form-group'),                                            
                                                    'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                                                    'class' => 'form-control'
                                                )));
            echo $this->Form->input('title',array('label' => 'Name of the Post'));
            echo $this->Form->input('max_posts',array('label'=>array('text' => 'No of posts')));
            echo $this->Form->input('election_id',array('type' => 'hidden','default' => $election_detail['Elections']['id']));
            echo $this->Form->end(array('label' => 'Add Post','div'=>false,'class'=>'btn btn-default','id' => 'addPost','data-loading-text' => "Adding..."));
            ?>
            </div>
        </div>
    </div>
    <div id="addCandidateForm" class="panel panel-default inlineforms">
        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Add Candidate</div>
        <div id="collapseThree" class="panel-collapse collapse">
            <div class="panel-body">
            <?php
            echo $this->Form->create('Candidates',array('class' => 'form-inline',
                                                'type' => 'file',
                                                'url' => array('controller' => 'Candidates','action' => 'add'),
                                                'inputDefaults' => array(
                                                    'format' => array('before', 'label', 'between','error','input','after'),
                                                    'div' => array('class' => 'form-group'),                                            
                                                    'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                                                    'class' => 'form-control'
                                                )));            
            echo $this->Form->input('name',array('label' => 'Name '));
            echo $this->Form->input('image_path',array('label' => '  Photo  ','type'=> 'file','accept' => 'image/*'));
            echo $this->Form->input('election_id',array('type' => 'hidden','default' => $election_detail['Elections']['id']));            
            echo $this->Form->end(array('label' => 'Add Candidate','div'=>false,'class'=>'btn btn-default','id' => 'addPost'));
            ?>
            </div>
        </div>
    </div>    
</div>
<div class="content">
    <h2>Election Candidates List</h2>
    <h4>Drag and drop candidates/posts to change the order and post</h4>
    <button id="saveList" class="btn btn-primary pull-right">Save</button>
    <form id="saveListForm" style="display:none" method="post">
        <input type="hidden" name="candidatesList"></input>
    </form>
    <ul class="ulnotype postList">
        <li data-id= "-1">
            <div class="title">
                <h3>Unlisted Candidates</h3>
            </div>
            <div class="candidates">
                <ul class="ulnotype">
                    <?php foreach($unlisted as $cand) { ?>
                    <li class="thumbnail" data-id="<?php echo $cand['Candidates']['id'] ?>">
                        <img src="<?php echo $this->webroot.$cand['Candidates']['image_path'] ?>">
                        <div class="rightside">
                            <h4 class="title"><?php echo $cand['Candidates']['name'];?></h4>
                        </div>
                    </li>
                    <?php } ?>                    
                </ul>
                <div style="clear:both"></div>
            </div>
        </li>
        <?php foreach($election_detail['Posts'] as $post){?>
        <li class="moveable" data-id="<?php echo $post['id'] ?>">
            <div class="title">
                <h3><?php echo $post['title'];?> (<?php echo $post['max_posts'];?> post)</h3>
            </div>
            <div class="candidates">
                <ul class="ulnotype">
                    <?php foreach($post['Candidates'] as $cand) { ?>
                    <li class="thumbnail" data-id="<?php echo $cand['id'] ?>">
                        <img src="<?php echo $this->webroot.$cand['image_path'] ?>">
                        <div class="rightside">
                            <h4 class="title"><?php echo $cand['name'];?></h4>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
                <div style="clear:both"></div>
            </div>
        </li>
        <?php } ?>
    </ul>
</div>
<?php 
echo $this->Html->script('edit.js');
?>