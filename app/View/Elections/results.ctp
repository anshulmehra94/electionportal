<h3>Election result : <?php echo $election['Elections']['title'] ?> </h3>
<hr>
<table class="table" style="text-align:center">
    <?php foreach($election['Posts'] as $post){?>
        <tr><th style="text-align:center" colspan="4"><?php echo $post['title'] ?></th></tr>
        <tr><td>Name</td><td>Yes</td><td>No</td><td>Neutral</td></tr>
        <?php foreach($post['Candidates'] as $cand) { ?>
            <tr><td><?php echo $cand['name'] ?></td><td><?php echo $cand['votes_yes'] ?></td><td><?php echo $cand['votes_no'] ?></td><td><?php echo $cand['votes_neutral'] ?></td></tr>
        <?php } ?>        
    <?php } ?>
</table>