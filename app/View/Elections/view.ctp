<div class="content">
    <?php if(!isset($voted)){ ?>
    <h2><?php echo $election_detail['Elections']['title']?></h2>    
    <hr>
    <div style="color:green" class="well">
        <h4>Instructions</h4>
        <ul>
            <li>You will need a secret key which is unique for each voter, if you don't have one please contact election officer</li>
            <li>For posts in which number of candidates are less than or equal to the number of posts, you have to give each candidate yes,no or neutral(default) vote</li>
            <li>For posts in which number of candidates are more than number of actual post, you need to select the candiates for which you want to vote and if you select no one your vote will be considered neutral</li>                            
            <li>For post separate for UG and PG, votes will be considered from voter corresponding to same category. 2 year MSC courses are also considered under UG</li>
        </ul>
    </div>
    <form method="post" id="votesform">        
        <input type="hidden" name="data[Votes][election_id]" value="<?php echo $election_detail['Elections']['id']; ?>">
        <ul class="ulnotype postList">        
            <?php foreach($election_detail['Posts'] as $post){?>
            <li class="moveable" >
                <div class="title">
                    <h3><?php echo $post['title'];?> (<?php echo $post['max_posts'];?> post)</h3>
                </div>
                <div class="candidates" <?php if(count($post['Candidates']) > $post['max_posts']) echo "data-max-posts = \"".$post['max_posts']."\""; ?> >
                    <ul class="ulnotype">
                        <?php foreach($post['Candidates'] as $cand) { ?>
                        <li class="thumbnail">
                            <img src="<?php echo $this->webroot.$cand['image_path'] ?>">
                            <div class="rightside">
                                <h4 class="title"><?php echo $cand['name'];?></h4>
                                <div class="inputbar">
                                    <?php if(count($post['Candidates']) > $post['max_posts']){?> 
                                        <input type="checkbox" name="data[Votes][<?php echo $post['id'];?>][]" value="<?php echo $cand['id'];?>">                                
                                    <?php } else{ ?>
                                        <label class="radio-inline">
                                            <input type="radio" name="data[Votes][<?php echo $post['id']."][".$cand['id'];?>]" value="yes"> Yes
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="data[Votes][<?php echo $post['id']."][".$cand['id'];?>]" value="no"> No
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="data[Votes][<?php echo $post['id']."][".$cand['id'];?>]" value="neutral" checked="checked"> Neutral
                                        </label>
                                    <?php }?>
                                </div>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                    <div style="clear:both"></div>
                </div>
            </li>
            <?php } ?>
        </ul>
        <hr>
        <?php echo $this->Session->flash('auth');?>
        <h3>Login Details</h3>   
        <div id="loginform">     
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Ldap Id</label>
                <div class="col-sm-4">
                  <input type="text" required="required" class="form-control" id="inputEmail3" name="data[Votes][ldap_id]" placeholder="Ldap ID">
                </div>
                <div style="clear:both"></div>
            </div>            
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-4">
                  <input type="password" required="required" class="form-control" id="inputPassword3" name="data[Votes][password]" placeholder="Password">
                </div>
                <div style="clear:both"></div>         
            </div>         
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Secret Key</label>
                <div class="col-sm-4">
                  <input type="text" required="required" class="form-control" id="inputEmail3" name="data[Votes][secret_key]" placeholder="Secret Key">
                </div>
                <div style="clear:both"></div>
            </div>            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                  <button type="submit" class="btn btn-default" data-loading-text="Cheking...">Vote</button>
                </div>
            </div>     
            <input type="hidden" id="checkCredUrl" value="<?php echo $this->Html->url(array('controller' => 'elections','action' => 'check_voter_cred',$election_detail['Elections']['id'])) ?>">
            <div style="clear:both"></div>
        </div>
    </form>    
    <?php } else { 
        header("Refresh: 5; url=".$this->here);
        ?>
        <h2>Your vote has been successfully recorded</h2>
        <h3>Thanks</h3>
        <h4>Riderection in <span id="counter">5</span> to the election page or <a href="<?php echo $this->here?>">click here</a></h4>
    <?php } ?>
</div>
<?php 
echo $this->Html->script('view.js');
?>