<div style="width:350px;margin:auto;text-align:center;margin-top: 200px">
    <h3>Log In</h3>
    
    <?php echo $this->Session->flash('auth1'); ?>
    <form method="post" class="form-horizontal">
        <div class="form-group">
            <label for="username" class="col-sm-3 control-label">Ldap ID</label>
            <div class="col-sm-9">
                <input type="text" name="data[users][username]" required="" id="username" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-3 control-label">Password</label>
            <div class="col-sm-9">
                <input type="password" name="data[users][password]" required="" id="password" class="form-control">
            </div>
        </div>
        <button type="submit" class="btn btn-default">Log In</button>
    </form>
</div>