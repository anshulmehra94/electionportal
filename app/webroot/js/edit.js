
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    $( ".postList" ).sortable({   
        items:".moveable"
    });
 
    $( ".candidates ul" ).each(function(){
        $(this).sortable({
            connectWith: ".candidates ul",
            dropOnEmpty: true
        });
    });
 
    $(".postList, .candidates ul" ).disableSelection();

    $("#saveList").click(function(){
        var data = "[";
        var count1 = $(".postList > li").length;

        $(".postList > li").each(function(){
            var i = parseInt($(this).attr('data-id'));
            data += "{\""+i+"\":[";
            var count  = $(this).find("ul li").length;

            $(this).find("ul li").each(function(){
                var i1 = parseInt($(this).attr('data-id'));
                if(--count)
                    data += i1+",";
                else
                    data += i1;

            });
            if(--count1)
                data += "]},";
            else
                data += "]}";
        });
        data += "]"
        //console.log(data);        
        $("#saveListForm input").val(encodeURIComponent(data));
        $("#saveListForm").submit();
    });

    //save button code
});


