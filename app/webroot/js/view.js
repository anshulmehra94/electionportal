
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    $(".candidates[data-max-posts]").each(function(){
        $(this).find('li input').click(function(e){
            e.stopPropagation();
        })
        $(this).find('li').click(function(){
            $(this).find('input[type=checkbox]').click();            
        })
        if(parseInt($(this).attr("data-max-posts")) > 1){
            var datamax = parseInt($(this).attr("data-max-posts"));
            $(this).find('li input[type=checkbox]').click(function(e){
                if($(this).closest('ul').find('li input[type=checkbox]:checked').length > datamax){
                    e.preventDefault();
                    alert("you can vote for maximum "+datamax+" candidates");                    
                }                
            })
        }
        else if(parseInt($(this).attr("data-max-posts")) == 1){
            $(this).find('li input[type=checkbox]').click(function(e){               
                var checked = false;
                if($(this).prop('checked'))
                    checked = true;
                $(this).closest('ul').find('li input[type=checkbox]:checked').prop('checked',false);
                $(this).prop('checked',checked);
            })    
        }
                  
    })

    var flag = false;
    $('#votesform').submit(function(e){
        if(!flag){
            e.preventDefault();        
            var that = this;
            $(this).find('button[type=submit]').button('loading');
            $.ajax({
                type : "POST",
                url : $(that).find('#checkCredUrl').val(),
                data : {'data[Auth][ldap_id]' : $(that).find('input[name="data[Votes][ldap_id]"]').val(),
                        'data[Auth][password]' : $(that).find('input[name="data[Votes][password]"]').val(),
                        'data[Auth][secret_key]' : $(that).find('input[name="data[Votes][secret_key]"]').val()
                        },
                success : function(data){
                    if(data == "true"){
                        flag = true;
                        $(that).submit();
                    }
                    else{
                        $(that).find('.alert').remove();
                        $(that).find('#loginform').before(data);
                    }
                    $(that).find('button[type=submit]').button('reset');
                },
                error : function(){
                    $(that).find('.alert').remove();
                    $(that).find('#loginform').before('<div class="alert alert-danger">Some error occurred, try again</div>');
                    $(that).find('button[type=submit]').button('reset');   
                }
            });    
        }
    });
    

    if($('#counter').length > 0){
        var counterInterval = setInterval(function(){
            var time = parseInt($('#counter').html());
            time--;
            $('#counter').html(time);
            if(time == 0 ){
                clearInterval(counterInterval);
                $('#counter').parent().find('a').click();
            }
        },1000);
    }
});


