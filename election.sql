-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 11, 2014 at 06:45 PM
-- Server version: 5.5.35
-- PHP Version: 5.3.10-1ubuntu3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `election`
--

-- --------------------------------------------------------

--
-- Table structure for table `portal_candidates`
--

DROP TABLE IF EXISTS `portal_candidates`;
CREATE TABLE IF NOT EXISTS `portal_candidates` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `post_id` int(10) DEFAULT NULL,
  `election_id` int(10) NOT NULL,
  `image_path` text NOT NULL,
  `votes_yes` int(10) NOT NULL DEFAULT '0',
  `votes_no` int(10) NOT NULL DEFAULT '0',
  `votes_neutral` int(10) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `election_id` (`election_id`),
  KEY `post_id` (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Table structure for table `portal_elections`
--

DROP TABLE IF EXISTS `portal_elections`;
CREATE TABLE IF NOT EXISTS `portal_elections` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `desc` text,
  `admin_id` int(10) NOT NULL,
  `mode` varchar(20) NOT NULL DEFAULT 'editing',
  `passcode` bigint(20) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `admin_id` (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `portal_posts`
--

DROP TABLE IF EXISTS `portal_posts`;
CREATE TABLE IF NOT EXISTS `portal_posts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `election_id` int(10) NOT NULL,
  `max_posts` int(3) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `election_id` (`election_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

-- --------------------------------------------------------

--
-- Table structure for table `portal_users`
--

DROP TABLE IF EXISTS `portal_users`;
CREATE TABLE IF NOT EXISTS `portal_users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ldapId` varchar(30) NOT NULL,
  `level` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ldap_id` (`ldapId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `portal_voters`
--

DROP TABLE IF EXISTS `portal_voters`;
CREATE TABLE IF NOT EXISTS `portal_voters` (
  `voter_id` varchar(15) NOT NULL,
  `is_voted` tinyint(1) NOT NULL DEFAULT '0',
  `secret` varchar(10) NOT NULL,
  `details` text NOT NULL,
  `election_id` int(10) NOT NULL,
  UNIQUE KEY `voter_id` (`voter_id`,`election_id`),
  KEY `election_id` (`election_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `portal_candidates`
--
ALTER TABLE `portal_candidates`
  ADD CONSTRAINT `portal_candidates_ibfk_2` FOREIGN KEY (`election_id`) REFERENCES `portal_elections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `portal_candidates_ibfk_4` FOREIGN KEY (`post_id`) REFERENCES `portal_posts` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `portal_elections`
--
ALTER TABLE `portal_elections`
  ADD CONSTRAINT `portal_elections_ibfk_4` FOREIGN KEY (`admin_id`) REFERENCES `portal_users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `portal_posts`
--
ALTER TABLE `portal_posts`
  ADD CONSTRAINT `portal_posts_ibfk_2` FOREIGN KEY (`election_id`) REFERENCES `portal_elections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `portal_voters`
--
ALTER TABLE `portal_voters`
  ADD CONSTRAINT `portal_voters_ibfk_1` FOREIGN KEY (`election_id`) REFERENCES `portal_elections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
